dhcpcd-dbus (0.6.1-4) unstable; urgency=medium

  * QA upload.
  * Migrate depends on dhcpcd5 to dhcpcd. (Closes: #1031093)

 -- Leandro Cunha <leandrocunha016@gmail.com>  Wed, 15 Feb 2023 18:27:03 -0300

dhcpcd-dbus (0.6.1-3) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 14:01:45 +0100

dhcpcd-dbus (0.6.1-2) unstable; urgency=medium

  * QA upload.
  * debian/control:
    - Add Vcs-Git and Vcs-Browser with repository in Salsa.
    - Bump Standards-Version to 4.6.0.1.
  * debian/copyright:
    - Fix license BSD-2 to BSD-2-clause in debian/copyright.
    - Update years of myself contribution.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Thu, 06 Jan 2022 02:06:43 -0300

dhcpcd-dbus (0.6.1-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * Fixed Lintian reports.
  * debian/control:
    - Bumped Standards-Version to 4.5.1.
    - Added Rules-Requires-Root: no.
    - Updated homepage field.
  * debian/watch:
    - Fixed problem to import tarball via uscan.
    - Updated version of 3 to 4.
  * debian/copyright:
    - Updated year upstream author.
    - Updated source field.
    - Updated file following DEP-5.
    - Added files debian/* and people involved with year of contribution.
    - Added myself.
  * debian/rules:
    - Set ignore dh_auto_test to fix FTBFS (Fails To Build From Source) and
      thanks to Simon McVitie maintainer of the dbus who helped me with this.
  * Added debian/gbp.conf.
  * Added debian/upstream/metadata.
  * Added debian/test/control to autopkgtest.
  * Added debian/salsa-ci.yml.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Wed, 30 Dec 2020 12:25:53 -0300

dhcpcd-dbus (0.6.0-2) unstable; urgency=medium

  * QA upload.
  * Set Debian QA Group as mantainer. (see #770080)
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control: bumped Standards-Version to 4.5.0.

 -- Deivite Huender Ribeiro Cardoso <deivite@riseup.net>  Sat, 09 May 2020 15:30:37 -0300

dhcpcd-dbus (0.6.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Don't run dh_auto_clean unless config.mk exists, (Closes: #724078)
    (from Andreas Moog <andreas.moog@warperbbs.de>)

 -- Chen Baozi <baozich@gmail.com>  Fri, 06 Jun 2014 13:04:26 +0800

dhcpcd-dbus (0.6.0-1) unstable; urgency=low

  * New upstream release

 -- Roy Marples <roy@marples.name>  Sun, 01 Apr 2012 17:00:19 +0100

dhcpcd-dbus (0.5.2-2) unstable; urgency=low

  Fix compilation on kFreeBSD

 -- Roy Marples <roy@marples.name>  Fri, 22 Oct 2010 19:57:34 +0100

dhcpcd-dbus (0.5.2-1) unstable; urgency=low

  * Initial upload to Debian (closes: #563974)

 -- Roy Marples <roy@marples.name>  Thu, 16 Sep 2010 10:37:38 +0100
